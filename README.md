## AC P2P - Engineering Essentials
### Week 1 - DevOps Assignment

This is a demo project based on Java Spring template modified with a simple REST endpoint /welcome/{name} and a custom pipeline, divided into three stages:

  - build (just a simple mvn package)
  - test (mvn test)
  - docker-hub-deploy (consists in docker login / docker pull / docker push)

### Visit the artifact on DockerHub [here](https://hub.docker.com/repository/docker/danillolobo/demo).

To test the endpoint locally just run the following command

```docker run -p 8080:5000 danillolobo/demo:master```

and then on your browser call the service endpoint

```localhost:8080/welcome/{name}```


## The information below is auto-generated kept for fairness

### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.
