package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @GetMapping("/")
    String home() {
        return "Week 1 assignment - DevOps with CI/CD!";
    }

    @GetMapping("welcome/{name}")
    String welcome(@PathVariable String name){
        return "Welcome ".concat(name).concat("!");
    }
}
